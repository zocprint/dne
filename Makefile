# Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

all: build 

build:
	go get -v
	go build -v

clean: 
	go clean
	rm -rf ../../../github.com/gorilla/mux
	rm -rf ../../../code.google.com/p/go-charset
	rm -f nohup.out