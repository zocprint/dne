// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"bitbucket.org/zocprint/dne/schema"
)

// Scan basedir searching for files that names match the File Prefix
// Struct Map and dispatch data loading for each one
func LoadAll(baseDir string) error {
	var err error

	verbose.Print("Loading 'Faixas CEPs UFs'... ")
	err = schema.LoadFaixasCEPUFs(baseDir)
	if err != nil {
		return err
	}
	verbose.Println("done.")

	verbose.Print("Loading 'Localidades'... ")
	err = schema.LoadLocalidades(baseDir)
	if err != nil {
		return err
	}
	verbose.Println("done.")

	verbose.Print("Loading 'Bairros'... ")
	err = schema.LoadBairros(baseDir)
	if err != nil {
		return err
	}
	verbose.Println("done.")

	verbose.Print("Loading 'Caixas Postais Comunitárias'... ")
	err = schema.LoadCaixasPostaisComunitárias(baseDir)
	if err != nil {
		return err
	}
	verbose.Println("done.")

	verbose.Print("Loading 'Logradouros'... ")
	err = schema.LoadLogradouros(baseDir)
	if err != nil {
		return err
	}
	verbose.Println("done.")

	verbose.Print("Loading 'Grandes Usuários'... ")
	err = schema.LoadGrandesUsuários(baseDir)
	if err != nil {
		return err
	}
	verbose.Println("done.")

	verbose.Print("Loading 'Unidades Operacionais'... ")
	err = schema.LoadUnidadesOperacionais(baseDir)
	if err != nil {
		return err
	}
	verbose.Println("done.")

	verbose.Println("DONE loading databases.")
	return nil
}
