# DNE SERVICE

The **dne** project is a HTTP server developed and maintained by [Zocprint](http://www.zocprint.com.br)
that loads all DNE database in **memory** to be served through a webservice. 

DNE is the acronym for *Diretório Nacional de Endereços*, which means 
*National Addresses Directory*. It's the main database for **Postal Codes** and **Addresses** in **Brazil** and is maintained and commercialized by [Correios](http://www.correios.com.br). That's why there is no data files in this repository.

## BRANCHES

 * master - used for development
 * go1.1 - used for production

## ENVIRONMENT SETUP

### Install Go

 * Download Go from [here](https://code.google.com/p/go/downloads/list)
 * Install Go following [this instructions](http://golang.org/doc/install#install)
 
#### Setup Global Environment Variables

Add these lines to your global shell login script: (`/etc/profile.d`, `/etc/profile`, etc)
 
    export GOROOT=/usr/local/go
    export GOBIN=$GOROOT/bin
    export PATH=$GOBIN:$PATH

#### Setup your Development Workspace

Create your Go workspace: 
    
	mkdir -p ~/Workspace/Go/src

Add the following line to your user's shell login script (eg. `~/.bashrc`), according to your **home folder**:
 
	export GOPATH="/home/foobar/Workspace/Go"
 
Check if everything is fine by running this:
 
	go env

## BUILDING
  
    go get bitbucket.org/zocprint/dne
	cd $GOPATH/src/bitbucket.org/zocprint/dne
	make clean
	make

### Jenkins

To setup a Jenkins job for a Go project, you need to use a customized Workspace. For example:

workspace/dne/src/bitbucket.org/zocprint/dne

And you need to setup the job to run the following script:

	#!/bin/bash
	set -x
	export GOPATH="$JENKINS_HOME/workspace/dne"
	export GOBIN="$GOPATH/bin"
	make clean
	make

You can also complement the job script to download the data files from your private repository and package them for deploy:

	mkdir -p bin
	rm -rf data
	git clone --depth=0 git@bitbucket.org:your-user/your-dne-data.git data
	rm -rf data/.git
	mv dne bin
	tar czf dne.tar.gz bin data

## USAGE

 * Just run **dne** with the path of the directory containing all official DNE data file (Delimited ones).
 * There is some flags to change **dne**'s behaviour. Just run `dne -h` to see the help.
 * **All data file's names and extensions must be uppercase**.
 * By default **dne** listens on TCP `127.0.0.1:16081`
 * The detailed webservice URL is `http://127.0.0.1:16081/cep/01234567` where `01234567` is a 7-digit CEP code
 * The quick webservice URL is `http://127.0.0.1:16081/quick/cep/01234567` where `01234567` is a 7-digit CEP code

### Upstart

 * Check the included upstart/dne.conf file. 
 * Change the paths according to your deployment preferences.
 * Place the dne.conf file in /etc/init/
 * Use `sudo start dne` to start the dne service or `sudo stop dne` to stop it

## DEPLOYMENT

There are two simple ways to deploy it to servers: 

 1) You can use a Jenkins job (or similar) to build a binary in
the same architecture/OS you use in your servers, and make it pack your binary, upstart script (if needded) and your DNE data files.

 2) Or you can deploy Go in your servers, use `go get bitbucket.org:zocprint/dne` and deploy only the DNE data files. `go get` will download the source code, build it and install it for you, inside $GOPATH (or $GOROOT).
 
# LICENSE

Copyright (c) 2013, **Zocprint Serviços Gráficos Ltda**.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * Neither the name of the organization nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL ZOCPRINT OR ITS CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

