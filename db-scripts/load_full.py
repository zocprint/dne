#!/usr/bin/env python

import sqlite3
import csv
import sys

basedir = 'delimited_files/full-1302'

def import_csv_to_table(conn, filename, table):
	c = conn.cursor()
	filepath = "%s/%s" % (basedir, filename)
	print("Loading '%s' into '%s'..." % (filepath, table))
	with open(filepath, 'r') as csvfile:
		reader = csv.reader(csvfile, delimiter='@')
		for row in reader:
			urow = [txt.decode('iso-8859-1') for txt in row]
			interrogs = ', '.join(['?'] * len(row))
			try:
				query = "INSERT INTO %s VALUES (%s)" % (table, interrogs)
				c.executemany(query, [urow])
			except sqlite3.Error as e:
				print '=' * 70
				print "Error: ", e, filename, table
				print query, urow
				print '=' * 70
	conn.commit()

conn = sqlite3.connect('dne.db')
c = conn.cursor()
c.execute('PRAGMA foreign_keys = ON')

import_csv_to_table(conn, 'LOG_FAIXA_UF.TXT', 'log_faixa_uf')
import_csv_to_table(conn, 'LOG_LOCALIDADE.TXT', 'log_localidade')
import_csv_to_table(conn, 'LOG_FAIXA_LOCALIDADE.TXT', 'log_faixa_localidade')
import_csv_to_table(conn, 'LOG_VAR_LOC.TXT', 'log_var_loc')
import_csv_to_table(conn, 'LOG_BAIRRO.TXT', 'log_bairro')
import_csv_to_table(conn, 'LOG_FAIXA_BAIRRO.TXT', 'log_faixa_bairro')
import_csv_to_table(conn, 'LOG_VAR_BAI.TXT', 'log_var_bai')
import_csv_to_table(conn, 'LOG_CPC.TXT', 'log_cpc')
import_csv_to_table(conn, 'LOG_FAIXA_CPC.TXT', 'log_faixa_cpc')
import_csv_to_table(conn, 'LOG_LOGRADOURO_AC.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_AL.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_AM.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_AP.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_BA.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_CE.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_DF.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_ES.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_GO.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_MA.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_MG.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_MS.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_MT.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_PA.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_PB.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_PE.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_PI.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_PR.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_RJ.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_RN.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_RO.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_RR.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_RS.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_SC.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_SE.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_SP.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_LOGRADOURO_TO.TXT', 'log_logradouro')
import_csv_to_table(conn, 'LOG_VAR_LOG.TXT', 'log_var_log')
import_csv_to_table(conn, 'LOG_NUM_SEC.TXT', 'log_num_sec')
import_csv_to_table(conn, 'LOG_GRANDE_USUARIO.TXT', 'log_grande_usuario')
import_csv_to_table(conn, 'LOG_UNID_OPER.TXT', 'log_unid_oper')
import_csv_to_table(conn, 'LOG_FAIXA_UOP.TXT', 'log_faixa_uop')

conn.close()
