#!/usr/bin/env python

import sqlite3

conn = sqlite3.connect('dne.db')
c = conn.cursor()
c.execute('PRAGMA foreign_keys = ON')

c.execute('''
	CREATE TABLE IF NOT EXISTS log_faixa_uf (
	  ufe_sg CHAR(2) NOT NULL,
	  ufe_cep_ini CHAR(8) NOT NULL,
	  ufe_cep_fim CHAR(8) NOT NULL,
	  PRIMARY KEY(ufe_sg, ufe_cep_ini)
	);
''')

c.execute('''
	CREATE TABLE IF NOT EXISTS log_localidade (
	  loc_nu INT(8) NOT NULL,
	  ufe_sg CHAR(2),
	  loc_no VARCHAR(72),
	  cep CHAR(8),
	  loc_in_sit CHAR(1),
	  loc_in_tipo_loc CHAR(1),
	  loc_nu_sub INT(8),
	  loc_no_abrev VARCHAR(36),
	  mun_nu CHAR(7),
	  PRIMARY KEY(loc_nu)
	);
''')

c.execute('''
	CREATE TABLE IF NOT EXISTS log_var_loc (
	  loc_nu INT(8) NOT NULL,
	  val_nu INT(8) NOT NULL,
	  val_tx VARCHAR(72),
	  PRIMARY KEY(loc_nu, val_nu),
	  FOREIGN KEY(loc_nu) REFERENCES log_localidade(loc_nu) ON DELETE CASCADE
	);
''')

c.execute('''
	CREATE TABLE IF NOT EXISTS log_faixa_localidade (
	  loc_nu INT(8) NOT NULL,
	  loc_cep_ini CHAR(8) NOT NULL,
	  loc_cep_fim CHAR(8),
	  PRIMARY KEY(loc_nu, loc_cep_ini),
	  FOREIGN KEY(loc_nu) REFERENCES log_localidade(loc_nu) ON DELETE CASCADE
	);
''')

c.execute('''
	CREATE TABLE IF NOT EXISTS log_bairro (
	  bai_nu INT(8) NOT NULL,
	  ufe_sg CHAR(2),
	  loc_nu INT(8),
	  bai_no VARCHAR(72),
	  bai_no_abrev VARCHAR(36),
	  PRIMARY KEY(bai_nu),
	  FOREIGN KEY(loc_nu) REFERENCES log_localidade(loc_nu) ON DELETE CASCADE
	);
''')

c.execute('''
	CREATE TABLE IF NOT EXISTS log_var_bai (
	  bai_nu INT(8) NOT NULL,
	  vdb_nu INT(8) NOT NULL,
	  vdb_tx VARCHAR(72),
	  PRIMARY KEY(bai_nu, vdb_nu),
	  FOREIGN KEY(bai_nu) REFERENCES log_bairro(bai_nu) ON DELETE CASCADE
	);
''')

c.execute('''
	CREATE TABLE IF NOT EXISTS log_faixa_bairro (
	  bai_nu INT(8) NOT NULL,
	  fcb_cep_ini CHAR(8),
	  fcb_cep_fim CHAR(8),
	  PRIMARY KEY(bai_nu, fcb_cep_ini),
	  FOREIGN KEY(bai_nu) REFERENCES log_bairro(bai_nu) ON DELETE CASCADE
	);
''')

c.execute('''
	CREATE TABLE IF NOT EXISTS log_cpc (
	  cpc_nu INT(8) NOT NULL,
	  ufe_sg CHAR(2),
	  loc_nu INT(8),
	  cpc_no VARCHAR(72),
	  cpc_endereco VARCHAR(100),
	  cep CHAR(8),
	  PRIMARY KEY(cpc_nu),
	  FOREIGN KEY(loc_nu) REFERENCES log_localidade(loc_nu) ON DELETE CASCADE
	);
''')

c.execute('''
	CREATE TABLE IF NOT EXISTS log_faixa_cpc (
	  cpc_nu INT(8) NOT NULL,
	  cpc_inicial VARCHAR(6),
	  cpc_final VARCHAR(6),
	  PRIMARY KEY(cpc_nu, cpc_inicial),
	  FOREIGN KEY(cpc_nu) REFERENCES log_cpc(cpc_nu) ON DELETE CASCADE
	);
''')

c.execute('''
	CREATE TABLE IF NOT EXISTS log_logradouro (
	  log_nu INT(8) NOT NULL,
	  ufe_sg CHAR(2),
	  loc_nu INT(8),
	  bai_nu_ini INT(8),
	  bai_nu_fim INT(8),
	  log_no VARCHAR(100),
	  log_complemento VARCHAR(100),
	  cep CHAR(8),
	  tlo_tx VARCHAR(36),
	  log_sta_tlo CHAR(1),
	  log_no_abrev VARCHAR(36),
	  PRIMARY KEY(log_nu),
	  FOREIGN KEY(loc_nu) REFERENCES log_localidade(loc_nu) ON DELETE CASCADE
	);
''')

c.execute('''
	CREATE TABLE IF NOT EXISTS log_var_log (
	  log_nu INT(8) NOT NULL,
	  vlo_nu INT(8) NOT NULL,
	  tlo_tx VARCHAR(36),
	  vlo_tx VARCHAR(150),
	  PRIMARY KEY(log_nu, vlo_nu),
	  FOREIGN KEY(log_nu) REFERENCES log_logradouro(log_nu) ON DELETE CASCADE
	);
''')

c.execute('''
	CREATE TABLE IF NOT EXISTS log_num_sec (
	  log_nu INT(8) NOT NULL,
	  sec_nu_ini VARCHAR(10),
	  sec_nu_fim VARCHAR(10),
	  sec_in_lado CHAR(1),
	  PRIMARY KEY(log_nu),
	  FOREIGN KEY(log_nu) REFERENCES log_logradouro(log_nu) ON DELETE CASCADE
	);
''')

c.execute('''
	CREATE TABLE IF NOT EXISTS log_grande_usuario (
	  gru_nu INT(8) NOT NULL,
	  ufe_sg CHAR(2),
	  loc_nu INT(8),
	  bai_nu INT(8),
	  log_nu INT(8),
	  gru_no VARCHAR(72),
	  gru_endereco VARCHAR(100),
	  cep CHAR(8),
	  gru_no_abrev VARCHAR(36),
	  PRIMARY KEY(gru_nu),
	  FOREIGN KEY(loc_nu) REFERENCES log_localidade(loc_nu) ON DELETE CASCADE
	);
''')

c.execute('''
	CREATE TABLE IF NOT EXISTS log_unid_oper (
	  uop_nu INT(8) NOT NULL,
	  ufe_sg CHAR(2),
	  loc_nu INT(8),
	  bai_nu INT(8),
	  log_nu INT(8),
	  uop_no VARCHAR(100),
	  uop_endereco VARCHAR(100),
	  cep CHAR(8),
	  uop_in_cp CHAR(1),
	  uop_no_abrev VARCHAR(36),
	  PRIMARY KEY(uop_nu),
	  FOREIGN KEY(loc_nu) REFERENCES log_localidade(loc_nu) ON DELETE CASCADE
	);
''')

c.execute('''
	CREATE TABLE IF NOT EXISTS log_faixa_uop (
	  uop_nu INT(8) NOT NULL,
	  fnc_inicial INT(8) NOT NULL,
	  fnc_final INT(8),
	  PRIMARY KEY(uop_nu, fnc_inicial),
	  FOREIGN KEY(uop_nu) REFERENCES log_unid_oper(uop_nu) ON DELETE CASCADE
	);
''')

conn.commit()
conn.close()
