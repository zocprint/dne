// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package schema

import (
	"io"
	"os"
	"path"
)

// LOG_CPC.TXT - Caixa Postal Comunitária (CPC) - são áreas rurais e/ou urbanas periféricas não atendidas pela distribuição domiciliária
type CaixaPostalComunitária struct {
	UF         *UF         `json:"-"` //     UFE_SG       sigla da UF          CHAR(2)
	Localidade *Localidade //     LOC_NU       chave da localidade  NUMBER(8)
	Nome       string      //     CPC_NO       nome                 VARCHAR(72)
	Endereço   string      //     CPC_ENDERECO endereço             VARCHAR(100)
	CEP        uint32      //     CEP          CEP                  CHAR(8)
	Faixas     []*FaixaCPC `json:"-"`
}

// LOG_FAIXA_CPC.TXT - Faixa de Caixa Postal Comunitária
type FaixaCPC struct {
	Inicial uint32 // PK  CPC_INICIAL  número inicial  VARCHAR(6)
	Final   uint32 //     CPC_FINAL    número final    VARCHAR(6)
}

type CaixaPostalComunitáriaMap map[uint32]*CaixaPostalComunitária

var (
	CaixasPostaisComunitárias = make(CaixaPostalComunitáriaMap)
	CEPCaixaPostalComunitária = make(CaixaPostalComunitáriaMap)
)

const (
	CAIXA_POSTAL_COMUNITARIA_FILE_NAME       = "LOG_CPC.TXT"
	FAIXA_CAIXA_POSTAL_COMUNITARIA_FILE_NAME = "LOG_FAIXA_CPC.TXT"
)

func NewCaixaPostalComunitáriaFromStrings(strs []string, filename string) (uint32, *CaixaPostalComunitária, error) {
	id, err := stringToUint32(strs[0])
	if err != nil {
		return 0, nil, err
	}
	if _, ok := UFs[strs[1]]; !ok {
		return id, nil, MissingEntityError("UF", strs[1], filename)
	}
	idl, err := stringToUint32(strs[2])
	if err != nil {
		return id, nil, err
	}
	loc, ok := Localidades[idl]
	if !ok {
		return id, nil, MissingEntityError("Localidade", strs[2], filename)
	}
	cep, err := stringToUint32(strs[5])
	if err != nil {
		return id, nil, err
	}
	return id, &CaixaPostalComunitária{
		UF:         UFs[strs[1]],
		Localidade: loc,
		Nome:       strs[3],
		Endereço:   strs[4],
		CEP:        cep,
		Faixas:     make([]*FaixaCPC, 0),
	}, nil
}

func LoadCaixasPostaisComunitárias(baseDir string) error {
	// Determine file name  to load
	fname := path.Join(baseDir, CAIXA_POSTAL_COMUNITARIA_FILE_NAME)
	// Open file
	file, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer file.Close()
	// Start the CSV reader
	reader, err := startReader(file)
	if err != nil {
		return err
	}
	// Read each line and convert it
	for {
		cols, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		id, cpc, err := NewCaixaPostalComunitáriaFromStrings(cols, CAIXA_POSTAL_COMUNITARIA_FILE_NAME)
		if err != nil {
			return err
		}
		CaixasPostaisComunitárias[id] = cpc
		CEPCaixaPostalComunitária[cpc.CEP] = cpc
	}
	return loadFaixasCPC(baseDir)
}

func NewFaixaCPCFromStrings(strs []string) (uint32, *FaixaCPC, error) {
	id, err := stringToUint32(strs[0])
	if err != nil {
		return 0, nil, err
	}
	ini, err := stringToUint32(strs[1])
	if err != nil {
		return id, nil, err
	}
	fin, err := stringToUint32(strs[2])
	if err != nil {
		return id, nil, err
	}
	return id, &FaixaCPC{
		Inicial: ini,
		Final:   fin,
	}, nil
}

func loadFaixasCPC(baseDir string) error {
	fname := path.Join(baseDir, FAIXA_CAIXA_POSTAL_COMUNITARIA_FILE_NAME)
	file, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer file.Close()
	reader, err := startReader(file)
	if err != nil {
		return err
	}
	for {
		cols, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		id, fxcpc, err := NewFaixaCPCFromStrings(cols)
		if err != nil {
			return err
		}
		if _, ok := CaixasPostaisComunitárias[id]; !ok {
			return MissingEntityError("Caixa Postal Comunitária", cols[0], FAIXA_CAIXA_POSTAL_COMUNITARIA_FILE_NAME)
		}
		CaixasPostaisComunitárias[id].Faixas = append(CaixasPostaisComunitárias[id].Faixas, fxcpc)
	}
	return nil
}
